/*
 * File: Breakout.java
 * -------------------
 * Name:
 * Section Leader:
 * 
 * This file will eventually implement the game of Breakout.
 */

import acm.graphics.*;
import acm.program.*;
import acm.util.*;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;

public class Breakout extends GraphicsProgram {

/** Width and height of application window in pixels */
	public static final int APPLICATION_WIDTH = 600;
	public static final int APPLICATION_HEIGHT = 600;

/** Dimensions of game board (usually the same) */
	private static final int WIDTH = APPLICATION_WIDTH;
	private static final int HEIGHT = APPLICATION_HEIGHT;

/** Dimensions of the paddle */
	private static final int PADDLE_WIDTH = 80;
	private static final int PADDLE_HEIGHT = 12;

/** Offset of the paddle up from the bottom */
	private static final int PADDLE_Y_OFFSET = 30;

/** Number of bricks per row */
	private static final int NBRICKS_PER_ROW = 10;

/** Number of rows of bricks */
	private static final int NBRICK_ROWS = 10;

/** Separation between bricks */
	private static final int BRICK_SEP = 4;

/** Width of a brick */
	private static final int BRICK_WIDTH =
	  (WIDTH - (NBRICKS_PER_ROW - 1) * BRICK_SEP) / NBRICKS_PER_ROW;

/** Height of a brick */
	private static final int BRICK_HEIGHT = 8;

/** Radius of the ball in pixels */
	private static final int BALL_RADIUS = 10;

/** Offset of the top brick row from the top */
	private static final int BRICK_Y_OFFSET = 70;

/** Number of turns */
	private static final int NTURNS = 3;
	
/* Method: run() */
/** Runs the Breakout program. */
	public void run() {
		/* The game will run until the player hits all the bricks or 
		 * after NTURNS attempts */
		for (int i=0; i<NTURNS; i++){
			setup();
			playGame();  // Stops whenever the ball hits the ground or 0 bricks remaining
			if(gameOver()){
				message("You have "+(NTURNS-(i+1))+" more turn(s)");
				waitForClick();
				removeAll();
			}
			if (brickCounter == 0) {   
				removeAll();
				message("You Won!!!");
				break;   //leave the for loop if there`s no brick and the user win
			}
		}
		/* If after NTURNS attempts there are still bricks to hit the game will stop
		 * and will be displayed the message "You lose" */
		if (brickCounter >0) {
			removeAll();
			message("You lose!");
		}
	}
	/** Sets up the configuration of the world before playing */
	private void setup() {
		setUpBricks();
		createPaddle();
		createBall();
	}
	/** Set up the bricks and their colors */
	private void setUpBricks(){
		brickCounter = NBRICK_ROWS*NBRICKS_PER_ROW; //initial number of bricks
		for (int i=0; i<NBRICK_ROWS; i++){           //counting rows
			for (int j=0; j<NBRICKS_PER_ROW; j++){   //counting bricks per rows
				/* coordinates for adding bricks on row starting from left edge +separation */
				int x = BRICK_SEP/2 + j*(BRICK_WIDTH+BRICK_SEP);
				/* coordinates for rows starting with the top + the offset */
				int y = BRICK_Y_OFFSET + i*(BRICK_HEIGHT+BRICK_SEP);
				brick = new GRect (x, y, BRICK_WIDTH, BRICK_HEIGHT);
				brick.setFilled(true);
				/* Setting the color of bricks based on the row they are */
				if (i < 2){   //setting Red color for the first 2 rows
					brick.setFillColor(Color.RED);
				} else if(i > 1 && i < 4){   //setting Orange color for thet rows 3&4
					brick.setFillColor(Color.ORANGE);
				} else if(i > 3 && i < 6) {  //setting Yellow color for the rows 5&6
					brick.setFillColor(Color.YELLOW);
				}else if (i >5 && i < 8){    //setting Green color for the rows 7&8
					brick.setFillColor(Color.GREEN);
				} else {                     //setting Cyan color for the rows 9&10
					brick.setFillColor(Color.CYAN);
				}
				add(brick);
			}
		}
	}
	/** Create and set initial position of the paddle */
	private void createPaddle() {
		/* Coordinates for initials position of the paddle 
		 * centered at 30 pixel above bottom */
		double  x = (getWidth() - PADDLE_WIDTH)/2;   
		double y = getHeight() - PADDLE_Y_OFFSET - PADDLE_HEIGHT ;
		paddle = new GRect(x, y, PADDLE_WIDTH, PADDLE_HEIGHT);
		paddle.setFilled(true);
		add(paddle);
		addMouseListeners();
	}

	/** Called whenever the mouse is moved with the button down */
	public void mouseDragged(MouseEvent e) {
		/* Move the paddle after the mouse move on x direction */
		if(e.getX() > 0 && e.getX() < getWidth() - PADDLE_WIDTH) {  //limit the paddle moves in canvas
			paddle.move(e.getX()-paddle.getX(), 0); 
		} 
	}
	/** Create and set initial position of the ball */
	private void createBall() {
		/* Coordinates for initial position of the ball in the 
		 * center of the screen */
		double x = (getWidth() - 2*BALL_RADIUS)/2;
		double y = (getHeight() - 2*BALL_RADIUS)/2;
		ball = new GOval(x, y, 2*BALL_RADIUS, 2*BALL_RADIUS);
		ball.setFilled(true);
		add(ball);
	}
	/** Set up the game play */
	private void playGame() {
		waitForClick();   //the game will start after one clicking
		setVelocity();
		while (!gameOver()){
			bounceBall();
			checkingForCollisions();
			pause(15);
			if (brickCounter == 0) break;
		} 
	}
	/** Set up initial velocity of the ball */
	private void setVelocity() {
		/* Setting velocity of the ball. */
		vy = 5.0; //Initially the ball should be heading downward
		/* For the game to be more animated x component of velocity will chosen randomly */
		vx = rgen.nextDouble(1.0, 4.0);
		if (rgen.nextBoolean(0.5)) vx = -vx;
	}
	/** Make the ball bounce. If it bounce off the top or bottom wall reverse vy. Symmetrically,
	 * bounce off the side walls, reverse the sign of vx. */
	private void bounceBall() {
		if (ball.getY() <= 0 || ball.getY() >= getHeight()-2*BALL_RADIUS) vy = -vy ;
		if (ball.getX() <= 0 || ball.getX() >= getWidth() -2*BALL_RADIUS) vx = -vx;
		ball.move(vx, vy);
	}
	/** Returns true when the ball hit the bottom, which means the end of the turn */
	private boolean gameOver() {
		return (ball.getY() >= getHeight() -2*BALL_RADIUS);
	}
	/** Check for collision of the ball with the paddle or a brick */
	private void checkingForCollisions() {
		GObject collider = getCollidingObject() ;
		/* If the collider isn`t null it means the ball collides with the paddle or a brick */
		if (collider != null) {
			if (collider == paddle){
				/* We need to make sure that the ball only bounces off the top part of the paddle  
	             * and also that it doesn't "stick" to it if different sides of the ball hit the 
	             * paddle quickly and get the ball "stuck" on the paddle. */
				if (ball.getY() >= getHeight() - PADDLE_Y_OFFSET - PADDLE_HEIGHT - BALL_RADIUS*2 && 
						ball.getY() < getHeight() - PADDLE_Y_OFFSET - PADDLE_HEIGHT - BALL_RADIUS*2 + vy){
					vy = -vy;
				}
			/* If the ball collides with a brick, the brick will be removed and vertical 
			 * direction of the ball inversed */
			} else {  
				remove(collider);
				brickCounter --; // Decrease the number of brick after each hit
				vy = -vy;
			}
		}
	}
	/** Check the four corner points on the square in which the ball is inscribed.
	 *  These points have the advantage of being outside the ball, which means 
	 *  getElementAt can`t return the ball itself, but nonetheless close enough to 
	 *  make it appear that collision have occured */
	private GObject getCollidingObject() {
		GObject gobj = getElementAt(ball.getX(), ball.getY());  //upper left corner
		if (gobj != null) {
			return gobj;
		} else {
			gobj = getElementAt(ball.getX()+2*BALL_RADIUS, ball.getY());  //upper right corner
			if (gobj != null) {
				return gobj;
			} else {
				gobj = getElementAt(ball.getX()+2*BALL_RADIUS, ball.getY()+2*BALL_RADIUS);  //lower right corner
				if (gobj != null) {
					return gobj;
				} else {
					gobj = getElementAt(ball.getX(), ball.getY()+2*BALL_RADIUS);  //lower left corner
					if(gobj != null){
						return gobj;
					} else return null;
				}
			}
		}
	}
	/** This method prints out messages for winning or loosing the game */
	private void message(String message){
		GLabel text = new GLabel(message);
		text.setFont("Times New Roman-30");
		if (message == "You Won!!!") {
			text.setFont("Comic Sans MS-100");
			text.setColor(Color.RED);
		}
		if (message == "You lose!") {
			text.setFont("Tahoma-70");
			text.setColor(Color.RED);
		}
		add (text, (getWidth()-text.getWidth())/2, (getHeight()-text.getAscent())/2);
	}
	

	/* Private instance variables */
	private RandomGenerator rgen = RandomGenerator.getInstance();
	private GRect brick;
	private GRect paddle;
	private GOval ball;
	private double vx, vy;   // x, y components of velocity of the ball
	private int brickCounter;  //Counts the bricks remaining
}